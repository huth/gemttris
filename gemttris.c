/*************************************************************************/
/*                          GEMTetris - Release 5                        */
/* * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * * */
/*                 written by:  R. Grothmann and T. Huth                 */
/*************************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <vdi.h>
#include <aes.h>
#include <time.h>
#include "gemttris.h"

#ifndef max
#define max(a,b) ((a)>(b)?(a):(b))
#define min(a,b) ((a)<(b)?(a):(b))
#endif

/*#ifndef WF_CURRXYWH
#define WF_CURRXYWH 5
#define WF_WORKXYWH 4
#endif*/
/*
#ifndef BEG_UPDATE
#define BEG_UPDATE 1
#define END_UPDATE 0
#endif
*/

#define F1 0x3b
#define F2 0x3c
#define F9 0x43
#define F10 0x44


/* For v_openvwk() */
short work_in[12], work_out[57]; /* VDI screen information */
	
/* handle for VDI calls */
short	vhandle;

/* width and height of characters */
short gl_hchar,gl_wchar,gl_hbox,gl_wbox;

OBJECT *menu_tree;             /* Resource-Address of the Menues */
short wi_handle;               /* Handle of the window */
short xdesk,ydesk,wdesk,hdesk; /* Desktop size */

/* Game variables */
#define ELEMENTS (CLOSER|MOVER|NAME|INFO)
short xw,yw,hw,ww, xstein,ystein,hstein,wstein;
short sit[18][28];	/* current situation plus two borderfields */
short back[18][28];	/* current situation without falling stone */
short st[4][4];	        /* falling stone */
short st1[4][4];

/* possible stones */
short fig[7][4][4] =
{
	0,0,0,0,
	1,1,1,1,
	0,0,0,0,
	0,0,0,0,

	0,0,0,0,
	0,2,2,2,
	0,2,0,0,
	0,0,0,0,

	0,0,0,0,
	3,3,3,0,
	0,0,3,0,
	0,0,0,0,

	0,0,0,0,
	0,4,4,4,
	0,0,4,0,
	0,0,0,0,

	0,0,0,0,
	0,0,5,5,
	0,5,5,0,
	0,0,0,0,

	0,0,0,0,
	0,6,6,0,
	0,0,6,6,
	0,0,0,0,

	0,0,0,0,
	0,7,7,0,
	0,7,7,0,
	0,0,0,0
};

short is,js;	/* position of the falling stone */
long delay,maxdelay=100,mindelay=0,speed;
short endeflag=0;
short playende=0;
long punkte=0, reihen=0;
short color;
typedef char SCORESTR[32]; 
SCORESTR namen[2][10];		/* Namen for the Highscore */
long score[2][10];
short puffer;			/* Use keyboard buffe? */
#define clear_buffer()  while(ev_constat()){ev_conin();}
short moved;			/* Stone moved ? */
time_t timer;

short keycode;

/* Some functions added by Thothy for compatibility reasons: */

/* Check if a key is pressed */
short ev_constat()
{
	short evt, msgbuf[8], i;
	evt=evnt_multi(MU_KEYBD|MU_TIMER, 0,0,0, 0,0,0,0,0, 0,0,0,0,0,
			msgbuf, 0,0, &i,&i,&i,&i,&keycode,&i);
	if (evt&MU_KEYBD)
		return -1;
	else
	{
		keycode=0;
		return 0;
	}
}


/* Get a key code */
unsigned short ev_conin()
{
	unsigned short retcode;
	if (keycode == 0)
		keycode = evnt_keybd();
	retcode = keycode;
	keycode=0;
	return retcode;
}

/* reverses bytes in string s */
void reverse(char *s)
{
	short c, i, j;

	for(i=0, j=strlen(s)-1; i<j; i++, j--)
	{
		c = s[i];
		s[i] = s[j];
		s[j] = c;
	}
} /* reverse */

/* converts integer to ascii: s[] had best be long enough. */
#define ltoa(a,b,c) itoa10(a,b)
void itoa10(long i, char *s )
{
	long cnt, sign;

	if (( sign = i) < 0)
		i = -i ;

	cnt = 0 ;
	do
	{
	    s[ cnt++ ] = i % 10 + '0' ;
	} while ((i /= 10) > 0);

	if (sign < 0)
		s[ cnt++ ] = '-';

	s[cnt] = NULL;
	reverse(s);
}

int cnt_clock()
{
	static count;
	count+=16;
	evnt_timer(1,0);
	return count;
}


int open_vwork(void)
/* Opens the screen for graphical io */
{
	int i;

	if (appl_init() == -1)
		return 0;

	for (i = 1; i < 10; i++)
		work_in[i] = 0;
	work_in[10]=2; /* Use Pixel-Coordinates */
	vhandle=graf_handle(&gl_wchar,&gl_hchar,&gl_wbox,&gl_hbox);
	work_in[0]=vhandle;
	v_opnvwk(work_in,&vhandle,work_out);

	return 1; /* Success! */
}

int close_vwork(void)
/* Closes the screen and unregisters application */
{
	v_clsvwk(vhandle);
	if (appl_exit()==0) return 1;
	return 0;
}

int dialog(short tree, short start)
/* A simple dialog function that draws, does and closes a dialog */
{
	OBJECT *tree_addr;
	short x,y,w,h,ret;
	if (rsrc_gaddr(R_TREE,tree,&tree_addr)==0) return 0;
	form_center(tree_addr,&x,&y,&w,&h);
	graf_mouse(M_OFF,0);
	form_dial(FMD_START,0,0,0,0,x,y,w,h);
	form_dial(FMD_GROW,0,0,0,0,x,y,w,h); /* obsolete */
	objc_draw(tree_addr,0,25,x,y,w,h);
	graf_mouse(M_ON,0);
	ret=form_do(tree_addr,start); /* User interaction */
	tree_addr[ret].ob_state &= ~SELECTED; /* unselect Exit-Object */
	graf_mouse(M_OFF,0);
	form_dial(FMD_SHRINK,0,0,0,0,x,y,w,h); /* obsolete */
	form_dial(FMD_FINISH,0,0,0,0,x,y,w,h);
	graf_mouse(M_ON,0);
	return ret; /* Return the exit button */
}

void show_info(void)
/* Program information dialog */
{
	OBJECT *info;
	if (rsrc_gaddr(R_TREE,INFOF,&info) == 0)
		return;
	dialog(INFOF,0);
}

void size_window(void)
{
	short w,h;
	wind_calc(1,ELEMENTS,xdesk,ydesk,wdesk,hdesk,&xw,&yw,&ww,&hw);
	hw=(hw/20)*20; 
	w=(((hw/20)*work_out[4])/work_out[3]+1)*10;
	if (w>ww) ww=(ww/20)*20; 
	else ww=w;
	hstein=hw/20; wstein=ww/10;
	wind_calc(0,ELEMENTS,xw,yw,ww,hw,&xw,&yw,&ww,&hw);
	xw=xdesk+(wdesk-ww)/2-7*gl_wbox;
	if (xw<0) xw=0;
	yw=ydesk+(hdesk-hw)/2;
	wind_calc(1,ELEMENTS,xw,yw,ww,hw,&xstein,&ystein,&w,&h);
}

int open_windows(void)
/* Opens a window in maximal size */
{
	char *wtitle=" GEMTetris ";
	char *noscore=" Score: 0  Lines: 0";
	short dx, dy, dw, dh;
	wind_get(0, WF_WORKXYWH, &xdesk, &ydesk, &wdesk, &hdesk);
		/* Get the desktop sizes */
	if ((wi_handle=wind_create(ELEMENTS, xdesk,ydesk,wdesk,hdesk))<0)
		/* Register a new window if possible */
	 { form_alert(1,"[3][No more windows available][Cancel]"); return 1;}
	graf_mouse(M_OFF,0);
	size_window() ;
	wind_set(wi_handle, WF_NAME, wtitle);
	wind_set(wi_handle, WF_INFO, noscore);
	wind_open(wi_handle,xw,yw,ww,hw); /* Open main window */
	graf_mouse(M_ON,0);
	return 0;
}

void close_windows(void)
/* Close the windows and delete them */
{
	wind_close(wi_handle); /* Close */
	wind_delete(wi_handle); /* Delete */
}

int rc_intersect(short x, short y, short w, short h,
	short *x1, short *y1, short *w1, short *h1)
/* Famous function that calculates the intersection rectangle of two input rectangles */
{
	short x2,y2;
	x2=max(x,*x1);
	y2=max(y,*y1);
	*w1=min(x+w,*x1+*w1)-x2;
	*h1=min(y+h,*y1+*h1)-y2;
	*x1=x2; *y1=y2;
	return ((*w1>0)&&(*h1>0));
}


void stein(short i, short j, short pattern)
{
	short c,r,box[4];
	box[0]=(i-4)*wstein+xstein;
	box[1]=(j-4)*hstein+ystein;
	box[2]=box[0]+wstein-1;
	box[3]=box[1]+hstein-1;
	if (color>1) 
	{
		vsf_interior(vhandle,0);
		vsf_perimeter(vhandle,1);
		vsf_color(vhandle,1);
		v_bar(vhandle, box);
		vsf_color(vhandle, pattern+1);
		vsf_interior(vhandle, 1);
		box[0]++; box[1]++; box[2]--; box[3]--;
		v_bar(vhandle,box);
	}
	else
	{
		vsf_color(vhandle, 1);
		vsf_perimeter(vhandle, 1);
		vsf_interior(vhandle, 2);
		vsf_style(vhandle, pattern);
	}
	v_bar(vhandle, box);
}


void leer(short i, short j)
{
	short c,r,box[4];

	box[0]=(i-4)*wstein+xstein;
	box[1]=(j-4)*hstein+ystein;
	box[2]=box[0]+wstein-1;
	box[3]=box[1]+hstein-1;
	vsf_color(vhandle,0);
	vsf_interior(vhandle,1);
	v_bar(vhandle,box);
}


void draw_all(void)
{
	short i,j;
	short x1,y1,w1,h1,box[4];

	wind_update(BEG_UPDATE);
	graf_mouse(M_OFF, 0L);

	wind_get(wi_handle,WF_FIRSTXYWH,&x1,&y1,&w1,&h1); /* first rectangle */
	while((w1>0)&&(h1>0))
	{
		box[0]=x1; box[1]=y1; box[2]=w1+x1-1; box[3]=h1+y1-1;
		vs_clip(vhandle,1,box); /* Use Clipping! */
		for (i=4; i<14; i++)
			for (j=4; j<24; j++)
				if (back[i][j])
					stein(i,j,back[i][j]);
				else
					leer(i,j);
		wind_get(wi_handle,WF_NEXTXYWH,&x1,&y1,&w1,&h1);  /* next rectangle */
	}

	box[0]=xdesk; box[1]=ydesk; box[2]=wdesk+xdesk; box[3]=ydesk+hdesk;
	vs_clip(vhandle,1,box);

	graf_mouse(M_ON, 0L);
	wind_update(END_UPDATE);
}


void draw_punkte(void)
{
	static char scorestr[40];
	sprintf(scorestr, " Score: %li  Lines: %li", punkte, reihen);
	wind_set(wi_handle, WF_INFO, scorestr);
}


void redraw(short p[4])
/* Redraw the window */
{
	short i,j;

	vsf_interior(vhandle,1); vsf_color(vhandle,0); v_bar(vhandle,p);
	for (i=4; i<14; i++)
	  for (j=4; j<24; j++)
	    if (back[i][j]) 
		stein(i,j,back[i][j]); 
}

void do_redraw(short x, short y, short w, short h)
/* Handle the general redraw mechanism, uses redraw() */
{
	short box[4];

	wind_update(BEG_UPDATE); graf_mouse(M_OFF, 0L);

	wind_get(wi_handle,WF_FIRSTXYWH,&box[0],&box[1],&box[2],&box[3]); /* first rectangle */
	while( box[2]>0 && box[3]>0 )
	{
		if (rc_intersect(x,y,w,h,&box[0],&box[1],&box[2],&box[3]))
		{
			box[2]+=box[0]-1; box[3]+=box[1]-1;
			vs_clip(vhandle,1,box); /* Use Clipping! */
			redraw(box); /* Redraw! */
		}
		wind_get(wi_handle,WF_NEXTXYWH,&box[0],&box[1],&box[2],&box[3]);
			/* next rectangle */
	}
	box[0]=xdesk; box[1]=ydesk; box[2]=wdesk+xdesk; box[3]=ydesk+hdesk;
	vs_clip(vhandle,1,box);

	graf_mouse(M_ON, 0L); wind_update(END_UPDATE);
}


void select_stone(void)
/* Selects a new stone at random */
{
	short i,j,n=(rand()%7);
	for (i=0; i<4; i++) 
		for (j=0; j<4; j++)
			st[j][i]=fig[n][i][j];
	is=6; js=2;	/* start position */
}


void turn_stone(void)
/* turn a stone */
{	short i,j,h;
	for (i=0; i<4; i++)
		for (j=0; j<4; j++) st1[i][j]=st[i][j];
	h=st[0][0]; st[0][0]=st[3][0]; st[3][0]=st[3][3]; 
	st[3][3]=st[0][3]; st[0][3]=h;
	h=st[0][1]; st[0][1]=st[2][0]; st[2][0]=st[3][2]; 
	st[3][2]=st[1][3]; st[1][3]=h;
	h=st[0][2]; st[0][2]=st[1][0]; st[1][0]=st[3][1]; 
	st[3][1]=st[2][3]; st[2][3]=h;
	h=st[1][1]; st[1][1]=st[2][1]; st[2][1]=st[2][2]; 
	st[2][2]=st[1][2]; st[1][2]=h;
}

void turn_back(void)
{	short i,j;
	for (i=0; i<4; i++)
		for (j=0; j<4; j++) st[i][j]=st1[i][j];
}


int try_stone(void)
/* test if a stone matches the position (is,js)  */
{	short i,j;
	for (i=0; i<4; i++)
		for (j=0; j<4; j++)
			if ((st[i][j]!=0)&&(back[is+i][js+j]!=0)) return 0;
	return 1;
}



void empty_board(void)
{
	short i,j;
	for (i=0; i<18; i++)
		for (j=0; j<28; j++)
			back[i][j]=((i<4)||(i>=14)||(j>=24));
}



void wait(void)
{
	evnt_timer(delay*4, 0);
}


int remove_voll(void)
/* Remove filled lines */
{
	short i,j,k,erg=0,voll,voll1=0;
	for (j=4; j<24; j++)
	{
		voll=1;
		for (i=4; i<14; i++)
			if (!back[i][j]) { voll=0; break; }
		if (voll)
		{
			for (k=j; k>=4; k--)
				for (i=4; i<14; i++) back[i][k]=back[i][k-1];
			voll1=1;
			++erg;
			wait();
		}
	}
	if (voll1) draw_all();
	return erg;
}


void best(void)
{
	OBJECT *o;
	short i,j;
	static SCORESTR name="unknown";
	static SCORESTR sc[10];

	if (rsrc_gaddr(R_TREE, BESTF, &o) == 0)
		return;
	o[BNAME].ob_spec.tedinfo->te_ptext = name;
	o[BNAME].ob_spec.tedinfo->te_txtlen = 16;
	o[BSPEED].ob_spec.free_string = (speed ? "(fast)   " : "(slow)   ");
	/* Insert highscore */
	for (i = 0; i < 10; i++)
	{
		o[B1+i].ob_spec.tedinfo->te_ptext = namen[speed][i];
		o[B1+i].ob_spec.tedinfo->te_txtlen = 16;
		ltoa(score[speed][i],sc[i],10);
		o[S1+i].ob_spec.tedinfo->te_ptext = sc[i];
		o[S1+i].ob_spec.tedinfo->te_txtlen = 8;
	}
	dialog(BESTF, 0);
	i = 0;
	while (i < 10 && score[speed][i] >= punkte)
		i++;
	if (i < 10)
	{	for (j = 9; j > i; j--) 
		{	score[speed][j]=score[speed][j-1];
			memcpy(namen[speed][j],namen[speed][j-1],32);
		}
		score[speed][i]=punkte;
		memcpy(namen[speed][i],name,32);
	}
}


void setdelay(void)
{
	if (maxdelay>40)
	 { maxdelay=40; menu_icheck(menu_tree, MENQUICK, 1); speed=1; }
	else
	 { maxdelay=80; menu_icheck(menu_tree, MENQUICK, 0); speed=0; }
}

void setcolors(void)
{
	if (color==1)
	 { color=2; menu_icheck(menu_tree,MENCOLOR,1); }
	else
	 { color=1; menu_icheck(menu_tree,MENCOLOR,0); }
}

void setpuffer(void)
{
	puffer=!puffer;
	menu_icheck(menu_tree,MENTAST,puffer);
}


void load_score(void)
{
	FILE *sco;
	short i, sp, x;
	char instr[64], *zstr;

	sco = fopen("gemttris.sco","r");
	if (!sco)
		return;

	for (sp=0; sp<2; sp++)
	{
		for (i=0; i<10; i++)
		{
			fgets(instr, 64, sco);
			x=strlen(instr);
			while(instr[x]<33 && x>0)
			{
				instr[x]=0;
				--x;
			}
			while(instr[x]!=' ' && x>0)
				--x;
			score[sp][i]=atoi(&instr[x+1]);
			instr[x]=0;
			strcpy(&namen[sp][i][0], instr);
		}
	}

	fclose(sco);
}

void save_score(void)
{
	FILE *sco;
	short i,sp;
	if ((int)(sco=fopen("gemttris.sco","w"))>0)
	{
		for (sp=0; sp<2; sp++)
		for (i=0; i<10; i++)
		{
			if (namen[sp][i][0]<'1') 
				strcpy(&namen[sp][i][0],"unknown");
			fprintf(sco,"%s %ld\n",&namen[sp][i][0],score[sp][i]); 
		}
		fclose(sco);
	}
}


void messages(short *msgbuf)
{
	short i;

	switch(msgbuf[0])
	{
	 case MN_SELECTED : /* Menue selected */
		{
			switch(msgbuf[4])
			{
			  case MENQUIT : endeflag=1; break; 
			  case MENINFO : show_info(); break;
			  case MENQUICK : setdelay(); break;
			  case MENCOLOR : setcolors(); break;
			  case MENTAST : setpuffer(); break;
			  case MENSCORE : save_score(); break;
			  case MENLOOK : punkte=0; best(); break;
			}
			menu_tnormal(menu_tree,msgbuf[3],1);
		}
	  case WM_REDRAW :
		do_redraw(msgbuf[4],msgbuf[5],msgbuf[6],msgbuf[7]);
		break;
	  case WM_CLOSED :
	  	endeflag=1;  	playende=1;
	  	break;
	  case WM_TOPPED :
		wind_set(msgbuf[3], WF_TOP, 0,0,0,0);
		break;
	  case WM_MOVED :
		wind_set(msgbuf[3], WF_CURRXYWH, msgbuf[4], msgbuf[5], msgbuf[6], msgbuf[7]);
		wind_calc(1, ELEMENTS, msgbuf[4], msgbuf[5], msgbuf[6], msgbuf[7], &xstein, &ystein, &i, &i);
		break;
	}
	
}


/* try to move a stone. */
int move_stone(void)
{
	short b[18][28],i,j,down;
	long taste;
	short x1,y1,w1,h1,box[4];

	/* Make a Backup of the old field with falling stone */
	for (i=4; i<14; i++)
		for (j=4; j<24; j++)
			b[i][j]=sit[i][j];
	/* Key pressed? */
	moved=0;
	if(!puffer) evnt_timer(2,0);
	if ( ev_constat() )
	{
		taste = ev_conin();
		switch (taste >> 8)
		{
			case 0x4d : taste='6'; break;
			case 0x4b : taste='4' ; break;
			case 0x50 : taste='2' ; break;
			case 0x48 : taste='5' ; break;
			case 0x44 : taste=1; break;
		}
		switch (taste&0x0FF)
		{
			case '6' : js++; down=try_stone(); js--;
				is++; if (!try_stone()) { is--; clear_buffer(); }
					else moved=1;
				if (!down) goto nofall;
				break;
			case '4' : js++; down=try_stone(); js--;
				is--; if (!try_stone()) { is++; clear_buffer(); }
					else moved=1; 
				if (!down) goto nofall;
				break;
			case '5' : turn_stone(); 
				if (!try_stone()) { turn_back(); clear_buffer(); }
					else moved=1; 
				break;
			case ' ' :
			case '2' :
				do { js++; punkte+=1; } 
					while (try_stone()) ;
				js--; punkte-=1; clear_buffer();
				break;
			case 1 :
			case 27 :
				playende=(form_alert(1,
					"[2][Paused...     ][Continue|Stop]")==2);
				break;
		}
	}
	/* Try to move the stone down */
	if (cnt_clock() > timer+delay)
	{
		js++; 
		if (!try_stone())
		{
			js--;
		}
		else
		{
			moved = 1;
			timer = cnt_clock();
		}
	}
	else
	{
		moved = 1;
	}
  nofall:
	/* remove falling stone */
	for (i=0; i<18; i++)
		for (j=0; j<28; j++)
			sit[i][j]=back[i][j];
	/* set it on a new position */
	for (i=0; i<4; i++)
		for (j=0; j<4; j++)
			if (st[i][j]) sit[is+i][js+j]=st[i][j];
	/* draw new field */
	wind_update(BEG_UPDATE);
	graf_mouse(M_OFF, 0L);
	wind_get(wi_handle,WF_FIRSTXYWH,&x1,&y1,&w1,&h1); /* first rectangle */
	while((w1>0)&&(h1>0))
	{
		box[0]=x1; box[1]=y1; box[2]=w1+x1-1; box[3]=h1+y1-1;
		vs_clip(vhandle,1,box); /* Use Clipping! */
		for (i=4; i<14; i++)
			for (j=4; j<24; j++)
				if (b[i][j]!=sit[i][j])
					if (sit[i][j])
						stein(i,j,sit[i][j]);
					else
						leer(i,j);
		wind_get(wi_handle,WF_NEXTXYWH,&x1,&y1,&w1,&h1);  /* next rectangle */
	}

	box[0]=xdesk; box[1]=ydesk;
	box[2]=wdesk+xdesk; box[3]=ydesk+hdesk;
	vs_clip(vhandle,1,box);

	graf_mouse(M_ON, 0L);
	wind_update(END_UPDATE);

	if ((!puffer)&&moved) clear_buffer(); 
	return moved;
}


void play(void)
{
	short i,j,p[4],x,y,w,h, msgbuf[8];

	menu_ienable(menu_tree, MENQUIT, 0);
	menu_ienable(menu_tree, MENQUICK, 0);
	menu_ienable(menu_tree, MENGO, 0);
	menu_ienable(menu_tree, MENSCORE, 0);
	menu_ienable(menu_tree, MENLOOK, 0);
	wind_get(wi_handle,WF_FIRSTXYWH,&x,&y,&w,&h);
	wind_get(wi_handle,WF_WORKXYWH,&xw,&yw,&ww,&hw);
	if ((w<ww)||(h<hw)) return;
	p[0]=xdesk; p[1]=ydesk; p[2]=wdesk+xdesk; p[3]=ydesk+hdesk;
	vs_clip(vhandle,1,p);
	empty_board();
	delay=maxdelay;
	punkte=0; reihen=0;
	draw_all();
	draw_punkte();
	playende=0;
	while (1)
	{
		select_stone();
		timer = cnt_clock();
		clear_buffer();
		if (!try_stone()) break;
		if (playende) break;
		while ((move_stone())&&(!playende))
		 {
		  j=evnt_multi(MU_MESAG|MU_TIMER, 0,0,0, 0,0,0,0,0, 0,0,0,0,0,
			msgbuf, 0,0, &i,&i,&i,&i,&i,&i);
		  if(j & MU_MESAG) messages(msgbuf);
		 }
		for (i=4; i<14; i++)
			for (j=0; j<24; j++)
				back[i][j]=sit[i][j];
		i=remove_voll();
		reihen+=i;
		switch(i)
		 {
		  case 1: punkte+=20; break;
		  case 2: punkte+=50; break;
		  case 3: punkte+=80; break;
		  case 4: punkte+=120; break;
		 }
		draw_punkte();
		wait();
		delay=min(max(mindelay,
			(maxdelay>50)?(100-punkte/200):(maxdelay-reihen/6)),maxdelay);
	}
	clear_buffer();
	best();
	menu_ienable(menu_tree, MENGO, 1);
	menu_ienable(menu_tree, MENSCORE, 1);
	menu_ienable(menu_tree, MENLOOK, 1);
	menu_ienable(menu_tree, MENQUIT, 1);
	menu_ienable(menu_tree, MENQUICK, 1);
}


void init(void)
{
	short i;

	empty_board();
	/* Set colors */
	if (work_out[13]<=2) menu_ienable(menu_tree,MENCOLOR,0);
	else {	menu_icheck(menu_tree,MENCOLOR,1); color=2; }

	/* Highscore=0 */
	for (i=0; i<10; i++)
	{
	 strcpy(&namen[0][i][0],"unknown"); 
	 strcpy(&namen[1][i][0],"unknown"); 
	 score[0][i]=0; score[1][i]=0;
	}
	load_score();
	/* Keyboard buffer on */
	menu_icheck(menu_tree, MENTAST, 1);
	puffer=1;
	setdelay(); /* fast */
}


void reinit(void)
{
	save_score();
}


/* main loop and event handler (reacts to aes-events) */
int main_loop(void)
{
	short msgbuf[8];
	short mx,my,mb,state,key,clicks,evt;
	if (rsrc_gaddr(R_TREE,MENMENU,&menu_tree)==0) return 1;
		/* menue resource addresse */
	graf_mouse(M_OFF,0);
	menu_bar(menu_tree,1); /* draw menue */
	if (open_windows()) return 1; /* open windows */
	graf_mouse(M_ON,0);
	graf_mouse(0, 0); /* Mouse as Arrow */
	init();
	do
	{
		evt=evnt_multi(MU_KEYBD|MU_MESAG,  /* which events? */
			1,3,1, /* 1 Mouse click, both buttons pressed */
			0,600,20,39,20,	/* first event rectangle */
			1,600,20,39,20, /* 2nd  " */
			msgbuf,
			30000,0,	/* 30 secunds timer */
			&mx,&my,&mb,&state,&key,&clicks);
		if (evt & MU_MESAG) /* aes Message ? */
		 {
		  if(msgbuf[0]==MN_SELECTED && msgbuf[4]==MENGO)
		    {
		     menu_tnormal(menu_tree,msgbuf[3],1);
		     play();
		    }
		   else
		    messages(msgbuf);
		 }
		if (evt & MU_KEYBD) /* key pressed? */
			{
			 if (key==0x2e03) endeflag=1; /* Control-C? */
			 switch (key)
			  {
			    case F1 : play(); break; 
			    case F10 : setdelay(); break;
			    case F9 : setpuffer(); break;
			    case F2 : punkte=0; best(); break;
			  }
			}	
		if (endeflag) 
			endeflag=(form_alert(1,"[2][Quit program?][OK|No]")==1);
	} while (!endeflag);
	graf_mouse(M_OFF,0);
	close_windows(); /* Close the windows*/
	menu_bar(menu_tree,0); /* Erease menu */
	graf_mouse(M_ON,0);
	reinit();
	return 0;
}


/* Main function */
main()
{
	srand(time(NULL)); /* Initialize the random number generator */

	if (!open_vwork()) /* init the gem */
	{
		form_alert(1,"[3][Init error][Cancel]");
		return -1;
	}

	if (rsrc_load("gemttris.rsc") == 0)  /* load the rsc */
	{
		form_alert(1,"[3][Resource not found][Cancel]");
		return close_vwork();
	}

	main_loop(); /* Main loop */
	rsrc_free(); /* Unload the rsc */
	return close_vwork(); /* close the gem screen */
}
