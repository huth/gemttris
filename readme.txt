                                                                October 2015
   GEMTetris v0.5

One day in the late 1990s, I browsed throught some Atari CD-ROMs and I found
a Tetris clone with source code. At that point in time, Caldera released
the source code of GEM under the GPL, so PC-GEM had a little revival,
called FreeGEM. To support this revival, I decided to adapt the GEMTetris
sources to FreeGEM: I cleaned up some Atari ST specific OS calls in the
sources, added some new features like a moveable window, and released a
binary for FreeGEM (using Pacific-C to compile).

Now, many years later, I've found the sources of GEMTetris again on my
hard disk and thought that it might be nice to make them available again
somewhere in the internet. So I've now made sure that they can be compiled
with AHCC on the Atari again and bundled up a new release for the ST.

The game should be hopefully pretty self-explanatory - you just got to
know that you can control the stones with the cursor keys or with the
keys '2', '4', '5' and '6'. Press ESC to abort your current game.

 Enjoy,
  Thomas
